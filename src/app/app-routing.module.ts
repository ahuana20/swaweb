import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UsersComponent } from './pages/admin/users/users.component';
import { ProductsComponent } from './pages/admin/products/products.component';
import { ShopproductComponent } from './shopproduct/shopproduct.component';
import { CartComponent } from './shopproduct/cart/cart.component';
import { LoginComponent } from './login/login.component';
import { GuardService } from './service/guard.service';
import { Not403Component } from './pages/not403/not403.component';
import { LoginService } from './service/login.service';
import { ModalidadComponent } from './pages/admin/modalidad/modalidad.component';
import { RegisterComponent } from './login/register/register.component';
import { ListadoComprasComponent } from './pages/admin/listado-compras/listado-compras.component';
import { AboutComponent } from './shared/about/about.component';
import { ContactenosComponent } from './shared/contactenos/contactenos.component';
import { IndexComponent } from './shared/index/index.component';
import { ServiciosComponent } from './shared/servicios/servicios.component';

import { DetalleProductoComponent } from './shopproduct/detalle-producto/detalle-producto.component';
import { ExisteProductoComponent } from './shopproduct/detalle-producto/existe-producto/existe-producto.component';
import { ProfileComponent } from './login/profile/profile.component';
import { ChangepasswordComponent } from './login/profile/changepassword/changepassword.component';
import { MyAccountComponent } from './login/profile/my-account/my-account.component';
import { EstadoComponent } from './pages/admin/estado/estado.component';
import { MybuyComponent } from './login/profile/mybuy/mybuy.component';
import { RecuperarComponent } from './login/recuperar/recuperar.component';
import { TokenComponent } from './login/recuperar/token/token.component';



const routes: Routes = [
  { path: 'profile', component: ProfileComponent,canActivate:[GuardService],children:[
    {path:'changepassword',component:ChangepasswordComponent},
    {path:'my-account',component:MyAccountComponent},
    {path:'my-buy',component:MybuyComponent},
  ]},

  { path: 'detalle/:id', component: DetalleProductoComponent},
  { path: 'notfound', component: ExisteProductoComponent},
  { path: 'servicios', component: ServiciosComponent},
  { path: 'inicio', component: IndexComponent},
  { path: 'about', component: AboutComponent},
  { path: 'contactenos', component: ContactenosComponent},
  { path: 'admin/users', component: UsersComponent,canActivate:[GuardService]},
  { path: 'admin/estado', component: EstadoComponent,canActivate:[GuardService]},

  { path: 'admin/modalidad', component: ModalidadComponent,canActivate:[GuardService]},
  { path: 'admin/productos', component: ProductsComponent,canActivate:[GuardService]},
  { path:'admin/listadoProducto',component:ListadoComprasComponent,canActivate:[GuardService]},
  { path: 'shop', component: ShopproductComponent},
  { path: 'cart', component: CartComponent },
  { path:'NOT-403',component:Not403Component},


  { path: 'login', component: LoginComponent },
  { path:'recuperar',component:RecuperarComponent,children:
   [
     {path:':token',component:TokenComponent}
   ]},
  { path: 'register', component: RegisterComponent },
  { path:'',redirectTo:'inicio',pathMatch:'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    {onSameUrlNavigation: 'reload'}
   )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
