
import { GuardService } from './service/guard.service';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './shared/menu/menu.component';
import { FooterComponent } from './shared/footer/footer.component';
import { UsersComponent } from './pages/admin/users/users.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdduserComponent } from './pages/admin/users/adduser/adduser.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ViewuserComponent } from './pages/admin/users/viewuser/viewuser.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { ProductsComponent } from './pages/admin/products/products.component';
import { AddproductComponent } from './pages/admin/products/addproduct/addproduct.component';
import { ViewproductComponent } from './pages/admin/products/viewproduct/viewproduct.component';
import { ShopproductComponent } from './shopproduct/shopproduct.component';
import { CartComponent } from './shopproduct/cart/cart.component';
import { LoginComponent } from './login/login.component';
import {JwtModule} from '@auth0/angular-jwt';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { Not403Component } from './pages/not403/not403.component';
import { ModalidadComponent } from './pages/admin/modalidad/modalidad.component';

import { AddmodalidadComponent } from './pages/admin/modalidad/addmodalidad/addmodalidad.component';
import { RegisterComponent } from './login/register/register.component';
import { ListadoComprasComponent } from './pages/admin/listado-compras/listado-compras.component';
import { ViewmodalidadComponent } from './pages/admin/modalidad/viewmodalidad/viewmodalidad.component';
import { IndexComponent } from './shared/index/index.component';
import { AboutComponent } from './shared/about/about.component';
import { ContactenosComponent } from './shared/contactenos/contactenos.component';
import { ServiciosComponent } from './shared/servicios/servicios.component';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { InterceptorService } from './service/interceptor.service';
import { DetalleProductoComponent } from './shopproduct/detalle-producto/detalle-producto.component';
import { ExisteProductoComponent } from './shopproduct/detalle-producto/existe-producto/existe-producto.component';
import { ProfileComponent } from './login/profile/profile.component';
import { ChangepasswordComponent } from './login/profile/changepassword/changepassword.component';
import { MenuProfileComponent } from './login/profile/menu-profile/menu-profile.component';
import { MyAccountComponent } from './login/profile/my-account/my-account.component';
import { EstadoComponent } from './pages/admin/estado/estado.component';
import { ViewestadoComponent } from './pages/admin/estado/viewestado/viewestado.component';
import { AddestadoComponent } from './pages/admin/estado/addestado/addestado.component';
import { ComprasDialogComponent } from './pages/admin/listado-compras/compras-dialog/compras-dialog.component';
import { ShowpasswordComponent } from './login/profile/changepassword/showpassword/showpassword.component';
import { MybuyComponent } from './login/profile/mybuy/mybuy.component';
import { RecuperarComponent } from './login/recuperar/recuperar.component';
import { TokenComponent } from './login/recuperar/token/token.component';



@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    FooterComponent,
    UsersComponent,
    AdduserComponent,
    ViewuserComponent,
    ProductsComponent,
    AddproductComponent,
    ViewproductComponent,
    ShopproductComponent,
    CartComponent,
    LoginComponent,
    Not403Component,
    ModalidadComponent,
    ViewmodalidadComponent,
    AddmodalidadComponent,
    RegisterComponent,
    ListadoComprasComponent,
    IndexComponent,
    AboutComponent,
    ContactenosComponent,
    ServiciosComponent,
    DetalleProductoComponent,
    ExisteProductoComponent,
    ProfileComponent,
    ChangepasswordComponent,
    MenuProfileComponent,
    MyAccountComponent,
    EstadoComponent,
    ViewestadoComponent,
    AddestadoComponent,
    ComprasDialogComponent,
    ShowpasswordComponent,
    MybuyComponent,
    RecuperarComponent,
    TokenComponent,
    
    


  ],
  imports: [
    JwtModule.forRoot({
      config: {
        tokenGetter:() => {
           return localStorage.getItem('access_token'); 
        },
      },
   }),
   
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NoopAnimationsModule,
    MaterialModule,
     
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},GuardService,
     {provide:MAT_DATE_LOCALE,useValue:'es-ES'},
     {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
