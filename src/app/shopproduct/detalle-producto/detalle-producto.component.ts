import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { ProductoService } from '../../service/producto.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ProductoModel } from '../../_model/producto.model';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.component.html',
  styleUrls: ['./detalle-producto.component.css']
})
export class DetalleProductoComponent implements OnInit {
  id:number;
  pro:ProductoModel;
  cartProducts: any;
  productoss: Array<ProductoModel>;
  constructor(private producto:ProductoService,private route:ActivatedRoute,
            private router:Router) {
   
  
 
   }
  
  ngOnInit(): void {
    this.route.params.subscribe((params:Params)=>{
      this.id=params['id']||null;
      this.obtener(this.id);
       
     
    })
  }
    obtener(id){
    this.producto.getProductFreeId(id).subscribe(data=>{
       this.pro=data;
      
       if(data==null){
        this.router.navigate(['/notfound']);
       }
    },error=>{
      this.router.navigate(['/notfound']);
    })
    
    }
  
    existe(id){
       
    }
    
  obten(){
   
    if(localStorage.getItem('cart')===null){
      this.cartProducts=[];
    }else{
     this.cartProducts=JSON.parse(localStorage.getItem('cart'));
    }
   
    return this.cartProducts
  }
  addToCart(productoId) {
    let data:any = localStorage.getItem('cart');
    data=JSON.parse(data);
    if (data !== null) {
      this.cartProducts = data
    
    }
   
    let id_producto;
    let productos;
    productos=this.obten();
    productos.forEach(prod => {
      if(prod.id===productoId){
          id_producto=prod.id
      }
    });
    if(id_producto===productoId){
          
          const itemAModificar = data.find(item=>item.id === productoId)
          itemAModificar.cantidad += 1;
          localStorage.setItem('cart', JSON.stringify(data))
          this.obten()
          alert('Se modifico la cantidad de Productos de Su carrito');
      return;
    }else{
      this.pro.cantidad =1;
      
      this.cartProducts.push(this.pro);
          
      this.updateCartData(this.cartProducts);
   
        localStorage.setItem('cart', JSON.stringify(this.cartProducts));
      alert('Se agrego Nuevo Producto a Su carrito de Compras');
        
    }
 
    localStorage.setItem('cart', JSON.stringify(this.cartProducts));
    
  }
  updateCartData(cartData) {
    this.cartProducts = cartData;
  }

}


