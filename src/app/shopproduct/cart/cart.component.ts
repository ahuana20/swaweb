import { Component, HostListener, OnInit } from '@angular/core';
import { ModalidadPago } from '../../_model/modalidadPago.model';
import { ModalidadService } from '../../service/modalidad.service';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { ConsultaListaProducto} from '../../_model/consultaListaProducto.model';
import { ShopCarModel } from '../../_model/car.mode';
import { DetalleCar } from '../../_model/detalle.model';
import { ProductoModel } from '../../_model/producto.model';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { CarService } from '../../service/car.service';
import { UserModel } from '../../_model/user.model';
import {default as decode} from 'jwt-decode';
import { TOKEN_NAME } from '../../local/var_constant';
import { UserService } from '../../service/user.service';
import { LoginService } from '../../service/login.service';
import Swal from 'sweetalert2';

import { InterceptorService } from '../../service/interceptor.service';
import { tokenNotExpired } from 'angular2-jwt';
import { EstadoModel } from '../../_model/estado.model';
import { ProductoService } from '../../service/producto.service';





@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  arrayCarrito=[
    
  ];

  montotal=0;
  cantidad=0;
  modalidad:ModalidadPago[]=[];
  form:UntypedFormGroup;
  moda:ModalidadPago;
  detalleCar:DetalleCar[]=[];
  productosSeleccionados:ProductoModel[]=[];
  idModalidadSeleccionado:number;
  carpShop:ShopCarModel;
  filterOptions:Observable<any[]>;
  filterOptionModalidad:Observable<any[]>;
  Mycontrol:UntypedFormControl=new UntypedFormControl();
  MyControlModa:UntypedFormControl=new UntypedFormControl();
  fecha:Date = new Date();
  constructor(private modadlidadService:ModalidadService,
            private carService:CarService,      
              private router: Router,
              private userService:UserService,
              private loginService:LoginService,
              private interceptor:InterceptorService,
              private productoService:ProductoService) {
    this.moda = new ModalidadPago();
    this.form = new UntypedFormGroup({
        'id':new UntypedFormControl(0),
        'descripcion': new UntypedFormControl('',Validators.required),

    })
    let data = localStorage.getItem('cart');
    if (this.arrayCarrito.length>0 ||data) {

      this.router.navigate(['/cart']);
    } else {
      alert('Debe tener productos en el carrito');
      this.router.navigate(['/shop']);
    }
     
   }

  ngOnInit(): void {
    this.validarProductos();
    //this.recepcionProducto();
    this.listarModalidad();
    //this.recepcionarMontotal();
    this.obtener();
  
  }
  obtener(){
   
    if(localStorage.getItem('cart')===null){
      this.arrayCarrito=[];
    }else{
     this.arrayCarrito=JSON.parse(localStorage.getItem('cart'));
    }
   
    return this.arrayCarrito
  }
 
 

  recepcionProducto(){
    let load = localStorage.getItem('cart');
    load=JSON.parse(load);
 
 for(let i=0;i<load.length;i++){
   return load[i]['id']
          
          }



  }
  listarModalidad(){
    this.modadlidadService.getModalidadFree().subscribe(data=>{
      this.modalidad = data;
  
    })
  }
 

  comprar(){
    if(!this.form.valid){
      alert('Ingrese un Mensaje')
      return;
    }
  

    if(this.loginService.estaLogeado()==true){
      let token1 = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
      if(!tokenNotExpired(TOKEN_NAME,token1.access_token)){
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Lo siento su Sesion Expiro ,Ingrese Nuevamente!',
          
        })
        sessionStorage.clear();
        this.router.navigate(['/login']);
        return;
      }
    let load = JSON.parse(localStorage.getItem(localStorage.key(0)))
    let pro:any[]=[];
    let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodeToken = decode(token.access_token);
      let rol = decodeToken['user_name'];
   const des = this.form.value['descripcion'];
    let modalidad = new  ModalidadPago();
    modalidad.id = this.idModalidadSeleccionado;
    
    let estado = new EstadoModel();
    estado.id=1

    let user = new UserModel();
    let c:number;
    this.userService.getUsers().subscribe(data=>{
      data.forEach(s=>{
        if(rol===s.username){
        c =s.idUsuarios
        user.idUsuarios = c;
        }
      })
    })
     
    this.carpShop = new ShopCarModel();
    this.carpShop.usuario=user;  
    this.carpShop.modalidadPago=modalidad;
    this.carpShop.montotal = this.precio();
    this.carpShop.cantidad = this.can();
    this.carpShop.fecha = this.fecha;
    this.carpShop.estado = estado
    let descripcion = new DetalleCar();
    descripcion.descripcion = des;
   
    this.detalleCar.push(descripcion);
    this.carpShop.detalleCar =this.detalleCar;
    let consultaProducto = new ConsultaListaProducto();
    let can;
     if(des && this.idModalidadSeleccionado){
        consultaProducto.shopcar=this.carpShop;
        for(let i = 0; i < load.length; i++) {   
          pro.push({id:load[i].id, 
            unidad:load[i].cantidad
          });
          
          
        }   
           
         consultaProducto.lstProducto=pro;
         
         setTimeout(()=>{
                           
        this.carService.addCar(consultaProducto).subscribe(data=>{
         console.log(consultaProducto);
         if(data==1){
          Swal.fire(
            'Gracias por su compra!',
            'Vuelta Pronto!',
            'success'
          ) 
          this.router.navigate(['shop']);
          localStorage.clear();
         } else{
  
         }                
   
     
        })
       },100)
      }
      
      setTimeout(()=>{
       
      },900)
  
   
    }
    else{
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Debe Iniciar Sesion con una Cuenta!',
       })
       this.router.navigate(['login']);
    }
}

validarProductos(){


}  

  remover(id){
      let productos;
      productos = this.obtener();
      productos.forEach((producto,indice) => {
        if(producto.id===id){
          productos.splice(indice,1);
        }
      });
      localStorage.setItem('cart', JSON.stringify(this.arrayCarrito));

      
     this.precio();

    

    if(this.arrayCarrito.length<=0){
        this.router.navigate(['shop'])     
        localStorage.clear();
    }
  
  }
     
  
  
  removeAll(){
    this.arrayCarrito=[];
    localStorage.clear();
    this.router.navigate(['shop']);
  }
  atras(){
    this.router.navigate(['shop'])
  }
  conseguirUsuario(){
 /*   let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodeToken = decode(token.access_token);
  
    let rol = decodeToken['user_name'];
   
    this.userService.getUsers().subscribe(data=>{
      data.forEach(s=>{
        if(rol===s.username){
          let u = new UserModel();
           u.idUsuarios = s.idUsuarios
           
        }
      })
    })
*/


   /*  let USER = new UserModel();
      if(this.loginService.estaLogeado()==true){
        this.userService.getId().subscribe(c=>{
          this.userService.getIDBASE(c).subscribe(s=>{
            
           USER.idUsuarios=s[0][0]
           this.carpShop.usuario=USER;
      })
        })
     
      }else{
       alert('Necesita Loguearse')
       this.router.navigate(['login']);
      }
     
  }*/
  }

  can(){
    return  Object.values(this.arrayCarrito).reduce((acc, { cantidad }) => acc + cantidad, 0)
  
  }
  precio(){
    return Object.values(this.arrayCarrito).reduce((acc, {cantidad, price}) => acc + cantidad * price ,0)
  }
 
  cambiarCantidad(id){
  let productos=this.obtener();
  let id_producto;
  let data:any = localStorage.getItem('cart');
    data=JSON.parse(data);
    productos.forEach(prod => {
      if(prod.id===id){
          id_producto=prod.id
      }    });
       if(id_producto===id){
        const itemAModificar = data.find(item=>item.id === id)
        itemAModificar.cantidad += 1;
        localStorage.setItem('cart', JSON.stringify(data));
        this.obtener();
      }
      
    }
    disminuirCantidad(id){
      let productos=this.obtener();
      let id_producto;
      let i;
      let data:any = localStorage.getItem('cart');
        data=JSON.parse(data);
        productos.forEach((prod) => {
          if(prod.id===id){
              id_producto=prod.id
               }});
           if(id_producto===id){
            const itemAModificar = data.find(item=>item.id === id)
            itemAModificar.cantidad -= 1;
            if(itemAModificar.cantidad===0){
               this.remover(id);
           }else{
            localStorage.setItem('cart', JSON.stringify(data));
            this.obtener();
           } 
          }
          
        }


  

  
}
