import { Component, OnInit } from '@angular/core';
import { ProductoModel } from '../_model/producto.model';
import { ProductoService } from '../service/producto.service';
import { Router } from '@angular/router';
import { ShopCarModel } from '../_model/car.mode';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';

import { JwtHelperService } from '@auth0/angular-jwt';
import { TOKEN_NAME } from '../local/var_constant';
import { tokenNotExpired } from 'angular2-jwt';
import { InterceptorService } from '../service/interceptor.service';

@Component({
  selector: 'app-shopproduct',
  templateUrl: './shopproduct.component.html',
  styleUrls: ['./shopproduct.component.css']
})
export class ShopproductComponent implements OnInit {
  producto: Array<ProductoModel>;
  productoRecieved: Array<ProductoModel>;
  cartProducts: any;
  //  
  P:ProductoModel;
  form:UntypedFormGroup;
  quanty:number;
  cartlist: ShopCarModel[];
  base:number=1;
  totalSum: number = 0;
  id;
  constructor(private router: Router, private httpClientService: ProductoService,
       ) { 
      this.P = new ProductoModel();
      this.form= new UntypedFormGroup({
        'cantidad':new UntypedFormControl('')
      })
     
  }

  ngOnInit(): void {
    this.httpClientService.getProductFree().subscribe(response=>{
          this.handleSuccessfulResponse(response)
         
    })
  
      
    
     //from localstorage retrieve the cart item
     let data = localStorage.getItem('cart');
     //if this is not null convert it to JSON else initialize it as empty
     if (data !== null) {
       this.cartProducts = JSON.parse(data);
       
  
     } else {
       this.cartProducts = [];
     }
      
    

  }
  cantidad(valor:number){
       

     
  }   

  handleSuccessfulResponse(response) {
    this.producto = new Array<ProductoModel>();
    //get books returned by the api call
    this.productoRecieved = response;
    for (const pro of this.productoRecieved) {

      const productwithRetrievedImageField = new ProductoModel();
      productwithRetrievedImageField.id = pro.id;
      productwithRetrievedImageField.name = pro.name;
      
      productwithRetrievedImageField.retrievedImage = 'data:image/jpeg;base64,' + pro.picByte;
      productwithRetrievedImageField.unidad = pro.unidad;
      productwithRetrievedImageField.price = pro.price;
      productwithRetrievedImageField.tipo = pro.tipo;
      productwithRetrievedImageField.picByte = pro.picByte;
      this.producto.push(productwithRetrievedImageField);
    }
  }


  addToCart(productoId) {
      
    let pro = this.producto.find(product => {
  
      return product.id === +productoId;
    });
    let data:any = localStorage.getItem('cart');
    data=JSON.parse(data);
    if (data !== null) {
      this.cartProducts = data
    
    }
   
    let id_producto;
    let productos;
    productos=this.obtener();
    productos.forEach(prod => {
      if(prod.id===productoId){
          id_producto=prod.id
      }
    });
    if(id_producto===productoId){
          
          const itemAModificar = data.find(item=>item.id === productoId)
          itemAModificar.cantidad += 1;
          localStorage.setItem('cart', JSON.stringify(data))
          this.obtener()
          alert('Se modifico la cantidad de Productos de Su carrito');
      return;
    }else{
      pro.cantidad =1;
      
      this.cartProducts.push(pro);
          
      this.updateCartData(this.cartProducts);
   
        localStorage.setItem('cart', JSON.stringify(this.cartProducts));
      alert('Se agrego Nuevo Producto a Su carrito de Compras');
        
    }
 
    localStorage.setItem('cart', JSON.stringify(this.cartProducts));
    
  }
  recorrerArreglo(){
      
  
  }

  goToCart() {
    let data = localStorage.getItem('cart');
    if (data !== null) {

      this.router.navigate(['/cart']);
    } else {
      alert('Debe tener productos en el carrito');
    }
     


  
  }
  updateCartData(cartData) {
    this.cartProducts = cartData;
  }
  emptyCart() {
    this.cartProducts = [];
    localStorage.clear();
  }

  existe(id) {
    return this.producto.find(producto => producto.id === id);
}
recepcionProducto(){
  let load = localStorage.getItem('cart');
    load=JSON.parse(load);
      
        for(let i=0;i<load.length;i++){
            console.log(load[i]['id'])
            
      }
   

}
obtenerCantidad(){
  let load = localStorage.getItem('cart');
    load=JSON.parse(load);
        if(load){
        for(let i=0;i<load.length;i++){
          return load[i]['cantidad']
            }
      }

    }

  obtener(){
   
    if(localStorage.getItem('cart')===null){
      this.cartProducts=[];
    }else{
     this.cartProducts=JSON.parse(localStorage.getItem('cart'));
    }
   
    return this.cartProducts
  }

 
  verDetalle(id:number){
    this.router.navigate(['/detalle',id])
  }
}
