import { Component, Input, OnInit, Output , EventEmitter} from '@angular/core';
import { UserModel } from '../../../../_model/user.model';
import { UserService } from '../../../../service/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-adduser',
  templateUrl: './adduser.component.html',
  styleUrls: ['./adduser.component.css']
})
export class AdduserComponent implements OnInit {
  @Input()
  user: UserModel
  form:UntypedFormGroup
 
  @Output()
  userAddedEvent = new EventEmitter();

  message: string;
  password: string;
  seleccion:any;
  constructor(private userService: UserService,
    private activedRoute: ActivatedRoute,
    private snack:MatSnackBar,
    private router: Router
    ) { 
      this.user = new UserModel();
      this.form = new UntypedFormGroup({
        'idUsuarios': new UntypedFormControl(0),
         'username': new UntypedFormControl({value:'',disabled: false}),
          'authority': new UntypedFormControl({value:'',disabled: false}), 
          'password': new UntypedFormControl({value:'',disabled: false}),
          'correo':new UntypedFormControl({value:'',disabled: false}),
          'celular':new UntypedFormControl({value:'',disabled: false}),
         'apellidos':new UntypedFormControl({value:'',disabled: false}),
         'estado':new UntypedFormControl({value:'',disabled: false}),
          'nombres':new UntypedFormControl({value:'',disabled: false}),
        
      })
      
    }

  ngOnInit() {
    this.activedRoute.queryParams.subscribe(data=>{
      if(data['action']=='edit'){
        this.form = new UntypedFormGroup({
          'username': new UntypedFormControl(this.user.username),
          'authority': new UntypedFormControl(this.user.authority),
          'password':new UntypedFormControl(this.user.password),
          'correo':new UntypedFormControl(this.user.correo),
          'celular':new UntypedFormControl(this.user.celular),
         'apellidos':new UntypedFormControl(this.user.apellidos),
         'estado':new UntypedFormControl(this.user.enabled),
          'nombres':new UntypedFormControl(this.user.nombres),
        });
        

      }else{
        this.form = new UntypedFormGroup({
     
          'idUsuarios': new UntypedFormControl(0),
           'username': new UntypedFormControl(''),
            'authority': new UntypedFormControl(''), 
            'password': new UntypedFormControl(''),
            'correo':new UntypedFormControl(''),
            'celular':new UntypedFormControl(''),
           'apellidos':new UntypedFormControl(''),
           'estado':new UntypedFormControl(''),
            'nombres':new UntypedFormControl(''),
                  })
        
                
      }
    })
  }

  addUser() {
    this.userService.addUser(this.user).subscribe(
      (user) => {
        this.userAddedEvent.emit();
        this.router.navigate(['admin', 'users']);
      }
    );
  }
   saveUser(){
    if(this.user.idUsuarios==null){
    this.registrar();
    }else{
    this.modificar();
    }
   }

  modificar(){
 this.user.username =this.form.value['username'];
 this.user.authority=this.form.value['authority'];
 this.user.password=this.form.value['password'];
 this.user.correo =this.form.value['correo']
 this.user.celular =this.form.value['celular'];
 this.user.apellidos =this.form.value['apellidos']
 this.user.enabled = this.form.value['estado'];
 this.user.nombres =this.form.value['nombres'];
 
 this.userService.updateProducto(this.user).subscribe(data=>{
   this.userAddedEvent.emit();
   this.router.navigate(['admin','users']);
   this.userService.mensaje.next("Se Modifico Correctamente");
 })
  }
  registrar(){
      
            
  this.user.username =this.form.value['username'];
 this.user.authority=this.form.value['authority'];
 this.user.password=this.form.value['password'];
 this.user.correo =this.form.value['correo']
 this.user.celular =this.form.value['celular'];
 this.user.apellidos =this.form.value['apellidos']
 this.user.enabled = this.form.value['estado'];
 this.user.nombres =this.form.value['nombres'];
 this.userService.addUser(this.user).subscribe(data=>{
   this.userAddedEvent.emit();
   this.router.navigate(['admin','users'])
   this.userService.mensaje.next("Se Registro Correctamente");
 })
  }
}
