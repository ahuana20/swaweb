import { Component, Input, OnInit, Output,EventEmitter,AfterViewInit } from '@angular/core';
import { UserModel } from '../../../../_model/user.model';
import { UserService } from '../../../../service/user.service';
import { ActivatedRoute, Params, Router, RouterStateSnapshot } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-viewuser',
  templateUrl: './viewuser.component.html',
  styleUrls: ['./viewuser.component.css']
})
export class ViewuserComponent implements OnInit {

  @Input()
  user: UserModel



  


  @Output()
  userDeletedEvent = new EventEmitter();

  constructor(private httpClientService: UserService,
    private router: Router,
    private activatedRoute:ActivatedRoute,
    private snack:MatSnackBar
    ) {
     
     }

  ngOnInit() {
    this.httpClientService.mensaje.subscribe((data)=>{
      this.snack.open(data,null,{duration:2000});
    })
  }

  deleteUser() {
    this.httpClientService.deleteUser(this.user.idUsuarios).subscribe(
      (user) => {
        this.userDeletedEvent.emit(true);
        this.router.navigate(['admin', 'users']);
        
      }
    );
  }
  editProducto() {
    this.router.navigate(['admin', 'users'], { skipLocationChange:true, queryParams: { action: 'edit', id: this.user.idUsuarios } });
     
  }  

}
