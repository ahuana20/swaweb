import { Component, OnInit, Output } from '@angular/core';
import { UserModel } from '../../../_model/user.model';
import { UserService } from '../../../service/user.service';
import { ActivatedRoute, Router } from '@angular/router';
import{Location} from '@angular/common';



@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  
  users: Array<UserModel>;
  selectedUser: UserModel;
  userRecieved: Array<UserModel>;
  action: string;
  selectedUserId:number;
  constructor(private httpClientService: UserService,
    private router: Router,
    private activedRoute: ActivatedRoute,
    public _location:Location ) { }

  ngOnInit() {
    this.refreshData();

  }

  refreshData() {
    this.httpClientService.getUsers().subscribe(
      response => this.handleSuccessfulResponse(response)
    
    );

    this.activedRoute.queryParams.subscribe(
      (params) => {
        this.action = params['action'];
        const id = params['id'];
        if (id) {
          this.selectedUser = this.users.find(book => {
            return book.idUsuarios === +id;
          });
      }
    }
    );
    }

  handleSuccessfulResponse(response) {
    this.users = new Array<UserModel>();
    this.userRecieved = response;
    for (const user of this.userRecieved) {
    const userwithRecivied = new UserModel();
    userwithRecivied.idUsuarios = user.idUsuarios;
    userwithRecivied.apellidos = user.apellidos, 
    userwithRecivied.authority = user.authority,
    userwithRecivied.celular = user.celular, 
    userwithRecivied.correo = user.correo, 
    userwithRecivied.enabled = user.enabled, 
    userwithRecivied.nombres = user.nombres, 
    userwithRecivied.password = user.password, 
    userwithRecivied.username  = user.username    
    this.users.push(userwithRecivied);

    }
  }

  viewUser(id:number) {
  
  this.router.navigate(['admin', 'users'],{skipLocationChange:true,queryParams:{id,action:'view'}});

  

  }

  addUser() {
    this.selectedUser = new UserModel();
    this.router.navigate(['admin', 'users'], { skipLocationChange:true,queryParams: { action: 'add' } });
  }
}
