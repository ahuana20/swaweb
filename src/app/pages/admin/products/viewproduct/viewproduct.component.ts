import { Component, Input, OnInit,Output, EventEmitter, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductoModel } from '../../../../_model/producto.model';
import { ProductoService } from '../../../../service/producto.service';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { AddproductComponent } from '../addproduct/addproduct.component';


@Component({
  selector: 'app-viewproduct',
  templateUrl: './viewproduct.component.html',
  styleUrls: ['./viewproduct.component.css']
})
export class ViewproductComponent implements OnInit {

  @Input()
  productos: ProductoModel;
  
  
  @Output()
  productoDeletedEvent = new EventEmitter();
  constructor(private httpClientService: ProductoService, 
    private router: Router,
    private activedRoute:ActivatedRoute,
    private snack:MatSnackBar,
   // @Inject(MAT_DIALOG_DATA) public data:ProductoModel,
    //public dialogRef: MatDialogRef<ViewproductComponent>,
    //public diaglogEdit:MatDialog
    ) { 
    //this.nuevaData();

    }
  

  ngOnInit(): void {
    this.httpClientService.mensaje.subscribe((data)=>{
      this.snack.open(data,null,{duration:2000});
    })
  }
 /* nuevaData(){
    this.productos = new ProductoModel();
    this.productos.id = this.data.id;
   this.httpClientService.getProductFreeId(this.productos.id).subscribe(data=>{
     this.productos.name = data.name;
     this.productos.price = data.price;
     this.productos.unidad =data.unidad;
     this.productos.retrievedImage =  'data:image/jpeg;base64,' + data.picByte;

   })
*/
deleteProducto(){
  this.httpClientService.deleteProductos(this.productos.id).subscribe(
    (book) => {
      this.productoDeletedEvent.emit(true);
      this.router.navigate(['admin', 'productos'] );
    }
  );
}


  
  editProducto() {
    this.router.navigate(['admin', 'productos'], { skipLocationChange:true, queryParams: { action: 'edit', id: this.productos.id } });
 /*   let dialogRef = this.diaglogEdit.open(AddproductComponent,{
      width:'100%',
      data:this.productos.id ,
      
    })
  this.dialogRef.close()
*/
  }

}
     

 /*  cerrar(){
   this.dialogRef.close();
   }
*/ 
  

