import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductoModel } from '../../../_model/producto.model';
import { ProductoService } from '../../../service/producto.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { AddproductComponent } from './addproduct/addproduct.component';
import { ViewproductComponent } from './viewproduct/viewproduct.component';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  productos: Array<ProductoModel>;
  selectedProducto: ProductoModel;
  productoRecieved: Array<ProductoModel>;
  dataSource:MatTableDataSource<ProductoModel>;
  
  displayedColumns= ["idProducto","nombre","precio","sphoto","tipo","unidad","Acciones"];
  action: string;
  constructor(private httpClientService: ProductoService,
    private activedRoute: ActivatedRoute,
    private router: Router,
    private dialog:MatDialog) { }

  ngOnInit() {
    this.refreshData();

  }
  refreshData() {
    this.httpClientService.getProductos().subscribe(
      (response) =>{ 
        this.handleSuccessfulResponse(response)
  
      }
    );

    this.activedRoute.queryParams.subscribe(
      (params) => {
        this.action = params['action'];
        const id = params['id'];
        if (id) {
          this.selectedProducto = this.productos.find(book => {
            return book.id === +id;
          });
      }
    }
    );
    }
  handleSuccessfulResponse(response) {
    this.productos = new Array<ProductoModel>();
    this.productoRecieved = response;
    for (const produ of this.productoRecieved) {
      const productowithRetrievedImageField = new ProductoModel();
      productowithRetrievedImageField.id = produ.id;
      productowithRetrievedImageField.name = produ.name;
      productowithRetrievedImageField.descripcion=produ.descripcion
      productowithRetrievedImageField.retrievedImage = 'data:image/jpeg;base64,' + produ.picByte;
      productowithRetrievedImageField.tipo = produ.tipo;
      productowithRetrievedImageField.price = produ.price;
      productowithRetrievedImageField.picByte=produ.picByte;
      productowithRetrievedImageField.unidad = produ.unidad;
      this.productos.push(productowithRetrievedImageField);
           
    }
    this.dataSource = new MatTableDataSource(this.productos);
  }
  addProducto() {
    this.selectedProducto = new ProductoModel();
    this.router.navigate(['admin', 'productos'], {skipLocationChange:true, queryParams: { action: 'add' } });
  }
  viewProducto(id: number) {
  
   this.router.navigate(['admin','productos'], {skipLocationChange:true, queryParams: { id, action: 'view' } });
 
  }
 
 
  

}
