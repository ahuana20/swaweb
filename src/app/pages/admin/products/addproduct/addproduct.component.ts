import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { ProductoModel } from '../../../../_model/producto.model';
import { ProductoService } from '../../../../service/producto.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef } from '@angular/material/dialog';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-addproduct',
  templateUrl: './addproduct.component.html',
  styleUrls: ['./addproduct.component.css']
})
export class AddproductComponent implements OnInit {
  @Input()
  productos: ProductoModel;
  form:UntypedFormGroup;
  private selectedFile;
  imgURL: any;
  @Output()
  productoAddedEvent = new EventEmitter();
  numeros=""
  validarnombres:string ="^[áéíóúÁÉÍÓÚaA-zZ\\s]*"
  validardescripcion:string ="^[áéíóúÁÉÍÓÚaA-zZ\\s\\d.]*"
  validarprecio:string="^\\d{0,8}(\\.\\d{1,4})?$"
  validarunidad:string="^[1-9]{1}\\d*$"
  constructor(private httpClientService: ProductoService,
    private activedRoute: ActivatedRoute,
    private router: Router,
    private httpClient: HttpClient,
    private snack:MatSnackBar,
   ) {

      this.productos = new ProductoModel();
      this.form = new UntypedFormGroup({
        'id': new UntypedFormControl(0),
        'name': new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarnombres)]),
        'price': new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarprecio)]),
        'tipo':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarnombres)]),
        'unidad':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarunidad)]),
        'descripcion':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validardescripcion)]),
      })   ;


     }

  ngOnInit() {
  
      this.activedRoute.queryParams.subscribe(data=>{
        if(data['action']=='edit'){
          this.form = new UntypedFormGroup({
            'name': new UntypedFormControl(this.productos.name,[Validators.required,Validators.pattern(this.validarnombres)]),
            'price': new UntypedFormControl(this.productos.price,[Validators.required,Validators.pattern(this.validarprecio)]),
            'tipo':new UntypedFormControl(this.productos.tipo,[Validators.required,Validators.pattern(this.validarnombres)]),
            'unidad':new UntypedFormControl(this.productos.unidad,[Validators.required,Validators.pattern(this.validarunidad)]),
            'descripcion':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validardescripcion)]),
          })  ; 
             }else{
            this.form = new UntypedFormGroup({
              'id': new UntypedFormControl(0),
              'name': new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarnombres)]),
              'price': new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarprecio)]),
              'tipo':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarnombres)]),
              'unidad':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarunidad)]),
              'descripcion':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validardescripcion)]),
            })   ;
          }
      })
      this.httpClientService.mensaje.subscribe((data)=>{
        this.snack.open(data,null,{duration:2000});
      })
    
  }
  public onFileChanged(event) {

    this.selectedFile = event.target.files[0];
    
    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event2) => {
      this.imgURL = reader.result;
      
    };
   
  }

  registrar(){
  
    this.productos.name = this.form.value['name'];
    this.productos.price = this.form.value['price'];
    this.productos.tipo = this.form.value['tipo'];
    this.productos.descripcion = this.form.value['descripcion'];
    this.productos.unidad = this.form.value['unidad'];
    const uploadData = new FormData();
     
     if(this.selectedFile==undefined||null){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Debe Selecciona Una Imagen',
     
      })
       setTimeout( ()=>{
        Swal.close()
       },900)
      return ;
    }
    uploadData.append('imageFile', this.selectedFile, this.selectedFile.name);
    this.selectedFile.imageName = this.selectedFile.name;
  
    this.httpClientService.subirImagen(uploadData).subscribe(response=>{
      if(response.status===200){
      this.httpClientService.addProductos(this.productos).subscribe(data=>{
          if(data===1){
            this.productoAddedEvent.emit();
            this.router.navigate(['admin', 'productos']);
            this.httpClientService.mensaje.next("Se Agrego Correctamente");
            console.log(this.form.value);
          }else{
            this.httpClientService.mensaje.next("No se Agrego Correctamente");
          }
        
      });
      console.log('Image uploaded successfully');
      }else{
        console.log('Image not uploaded successfully');
      }
    })
    console.log(this.selectedFile,this.selectedFile.name)
  }
  
  modificar(){
    this.productos.name = this.form.value['name'];
    this.productos.price = this.form.value['price'];
    this.productos.tipo = this.form.value['tipo'];
    this.productos.unidad = this.form.value['unidad'];
    this.productos.descripcion = this.form.value['descripcion'];
    this.httpClientService.updateProducto(this.productos).subscribe(producto=>{
      this.productoAddedEvent.emit();
      this.router.navigate(['admin', 'productos']);
      this.httpClientService.mensaje.next("Se Modifico Correctamente");
    })
  }
  saveProducto(){
    
    if (this.productos.id == null) {
    this.registrar()
    }else{
      this.modificar();
    }}
  
    validar(campo:string){
      return this.form.get(campo)?.invalid &&
      this.form.get(campo)?.touched
    }

  }

/*  saveProducto() {
    this.productos.name = this.form.value['name'];
    this.productos.price = this.form.value['price'];
    this.productos.tipo = this.form.value['tipo'];
    this.productos.unidad = this.form.value['unidad'];
    if (this.productos.id == null) {
    const uploadData = new FormData();
    uploadData.append('imageFile', this.selectedFile, this.selectedFile.name);
    this.selectedFile.imageName = this.selectedFile.name;
 
    this.httpClient.post('http://localhost:8081/productos/upload', uploadData, { observe: 'response' })
      .subscribe((response) => {
        if (response.status === 200) {
          this.httpClientService.addProductos(this.productos).subscribe(
            (book) => {
              this.productoAddedEvent.emit();
              this.router.navigate(['admin', 'productos']);
              this.httpClientService.mensaje.next("Se Agrego Correctamente");
            }
          );
          console.log('Image uploaded successfully');
        } else {
          console.log('Image not uploaded successfully');
        }
      }
      );
  } else {
    this.httpClientService.updateProducto(this.productos).subscribe(
      (book) => {
        this.productoAddedEvent.emit();
        this.router.navigate(['admin', 'productos']);
        this.httpClientService.mensaje.next("Se Modifico Correctamente");
      }
    );
  }

}
*/




  



  
