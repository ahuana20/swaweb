import { Component, OnInit, ViewChild } from '@angular/core';
import { CarService } from '../../../service/car.service';
import { ShopCarModel } from '../../../_model/car.mode';
import { MatTableDataSource } from '@angular/material/table';
import { UntypedFormGroup, UntypedFormControl } from '@angular/forms';
import { FiltroCompra } from '../../../_model/filtroConsulta.model';
import { ComprasDialogComponent } from './compras-dialog/compras-dialog.component';
import { MatDialog } from '@angular/material/dialog';
import { carList } from '../../../_model/carListInterface';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatSort } from '@angular/material/sort';


@Component({
  selector: 'app-listado-compras',
  templateUrl: './listado-compras.component.html',
  styleUrls: ['./listado-compras.component.css']
})

export class ListadoComprasComponent implements OnInit {
  @ViewChild(MatSort) matSort:MatSort;
  form:UntypedFormGroup;
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol','username','nombrescompletos','modalidad','productos','estado','Acciones'];
  dataSource:MatTableDataSource<carList>;
  objeto:[]=[];
  car:carList[]=[]
  fecha:Date = new Date()
  constructor(private carservice:CarService,
    private dialog:MatDialog) { 
     this.form = new UntypedFormGroup({
       'usuario':new UntypedFormControl(''),
       'nombres':new UntypedFormControl(''),
       'fecha':new UntypedFormControl(''),
     })  
     
  
    
        
   
  }
  forEach(data){
    this.car=[]
    for (const produ of data) {
      const d = new carList();
      d.id_car = produ[0]
      d.cantidadTotal=produ[1]
      d.fecha=produ[2]
      d.montototal=produ[3]
      d.username=produ[4]
      d.nombres=produ[5]
      d.modalidad=produ[6]
      d.cantidadproductos=produ[7]
      d.estado=produ[8]
      this.car.push(d)
     
  }
}
  subscribeSort(){
    
    this.carservice.carCambio.subscribe((data:carList[])=>{
    
      this.forEach(data);
      this.dataSource= new MatTableDataSource(this.car);
      this.dataSource.sort=this.matSort;
    
    })
  /**
   *   this.service.pagoCambio.subscribe(data=>{
      this.lista=data;
      this.dataSource=new MatTableDataSource(this.lista);
      this.dataSource.sort=this.sort; 
      console.log(this.lista);
    })
   * 
   * 
   */

  }

  carList(){
    

    
    this.carservice.getListarProducto().subscribe( (data)=>{       
           this.forEach(data)

      
      this.dataSource= new MatTableDataSource(this.car);
      
    })

  }
 
  ngOnInit(): void {
  this.carList();

    this.subscribeSort();
  }
  

    listarTodo(){
     this.car=[];
     this.carList();
     
   
  
          
    }  

    buscar(){
      let filtro = new FiltroCompra(this.form.value['usuario'],this.form.value['nombres'],this.form.value['fecha'])
        if(!filtro){
          return;
        }
      
      if(filtro.fecha){
         filtro.fecha.setHours(0)
         filtro.fecha.setMinutes(0)
         filtro.fecha.setSeconds(0)
         filtro.fecha.setMilliseconds(0)
       
        delete filtro.username;
        delete filtro.nombres;
        
        this.carservice.buscar(filtro).subscribe(data=>{
          this.car=[]
          this.forEach(data)
          this.dataSource= new MatTableDataSource(this.car);
       
    
        });
       }else{
         delete filtro.fecha;
         
         if(filtro.username.length===0){
            delete filtro.username;
            
         }
         if(filtro.nombres.length===0){
           delete filtro.nombres;
          
         }
        
         this.carservice.buscar(filtro).subscribe(data=>{
           this.car=[]
           this.forEach(data)
           this.dataSource= new MatTableDataSource(this.car);
       
        
        });
       }
        this.form.reset({
          'usuario': '',
          'nombres': '',
          'fecha': '',
        });
    }
   
    verDetalleDialog(id){
    
      let dialogRef = this.dialog.open(ComprasDialogComponent,{
        width:'500px',
        data:id,
        disableClose:false
      }).afterClosed().subscribe(p=>{
        this.listarTodo();
      })
}

  }
