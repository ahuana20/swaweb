import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { carList } from '../../../../_model/carListInterface';
import { EstadoService } from '../../../../service/estado.service';
import { EstadoModel } from '../../../../_model/estado.model';
import { CarService } from '../../../../service/car.service';
import { updateCar } from '../../../../_model/UpdateCar';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-compras-dialog',
  templateUrl: './compras-dialog.component.html',
  styleUrls: ['./compras-dialog.component.css']
})
export class ComprasDialogComponent implements OnInit {

  car:carList
  estadoModel:EstadoModel[]=[];
  idEstadoSeleccionado:number;
  updateCar:updateCar;
  constructor(  private dialogRef : MatDialogRef<ComprasDialogComponent>,
                @Inject(MAT_DIALOG_DATA) private data,
                private estado:EstadoService,
                private carUpdate:CarService,
                ) { }

  ngOnInit(): void {
    this.car = new carList();
    this.car.id_car = this.data.id_car;
  this.car.estado = this.data.estado;

  this.listarModalidad();
  }
  listarModalidad(){
    this.estado.getEstado().subscribe(data=>{
      this.estadoModel = data;
      console.log(this.estadoModel);
    })
  }

  aceptar(){
    this.updateCar= new updateCar();
    this.updateCar.car= parseInt(this.data.id_car)
    this.updateCar.estado=this.idEstadoSeleccionado
    console.log(this.updateCar)
    console.log(this.idEstadoSeleccionado);
    if(this.idEstadoSeleccionado){
      this.carUpdate.updateCar(this.updateCar).subscribe(data=>{
   
      })
 
      this.dialogRef.close();
    }
    else{
      return
    }
 
  }
  cerrar(){
    this.dialogRef.close();
  }
}
