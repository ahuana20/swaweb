import { Component, OnInit } from '@angular/core';
import { EstadoModel } from '../../../_model/estado.model';
import { EstadoService } from '../../../service/estado.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-estado',
  templateUrl: './estado.component.html',
  styleUrls: ['./estado.component.css']
})
export class EstadoComponent implements OnInit {
  estado:Array<EstadoModel>;
  selectedEstado:EstadoModel
  dataSource:MatTableDataSource<EstadoModel>;
  displayedColumns= ["id","Nombre","Acciones"];
  action:string
  estadoRecieved: Array<EstadoModel>;
  constructor(private httpClienteService:EstadoService,
              private activatedRoute:ActivatedRoute,
              private router:Router,  
              ) { }

  ngOnInit() {
    this.refreshData();

  }
  refreshData() {
    this.httpClienteService.getEstado().subscribe(response => 
     
      this.handleSuccessfulResponse(response)
         
    );

    this.activatedRoute.queryParams.subscribe(
      (params) => {
        this.action = params['action'];
        console.log(this.action)
        const id = params['id'];
        if (id) {
          this.selectedEstado = this.estado.find(book => {
            return book.id === +id;
          });
      }
    }
    );
    }

  handleSuccessfulResponse(response){
    this.estado = new Array<EstadoModel>();
    this.estadoRecieved = response;
    for(const esta of this.estadoRecieved){
      const estadoRetrivewd = new EstadoModel();
      estadoRetrivewd.id = esta.id;
      estadoRetrivewd.estado = esta.estado;

      this.estado.push(estadoRetrivewd);
    }
  
    this.dataSource = new MatTableDataSource(this.estado)
  }
  addEstado(){
    this.selectedEstado = new EstadoModel();
    this.router.navigate(['admin', 'estado'], {skipLocationChange:true, queryParams: { action: 'add' } });

  }
  viewEstado(id:number){
    console.log(id);
    this.router.navigate(['admin','estado'], {skipLocationChange:true, queryParams: { id, action: 'view' } });
  }
}
