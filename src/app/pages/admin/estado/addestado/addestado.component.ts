import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { EstadoModel } from '../../../../_model/estado.model';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { EstadoService } from '../../../../service/estado.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-addestado',
  templateUrl: './addestado.component.html',
  styleUrls: ['./addestado.component.css']
})
export class AddestadoComponent implements OnInit {
  @Input()
  estados:EstadoModel;
  form:UntypedFormGroup;
  validarnombres:string ="^[áéíóúÁÉÍÓÚaA-zZ\\s]*"
  @Output()
  estadoAddedEvent = new EventEmitter();

  constructor(private httpClientService:EstadoService,
              private activedRoutte:ActivatedRoute,
              private snack:MatSnackBar,
              private router:Router) { 
              
                this.estados= new EstadoModel();
                this.form= new UntypedFormGroup({
                  'id':new UntypedFormControl(0),
                  'estado':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarnombres)])
                })
              }

  ngOnInit(): void {
    this.activedRoutte.queryParams.subscribe(data=>{
      if(data['action']=='edit'){
        this.form=new UntypedFormGroup({
          'estado':new UntypedFormControl(this.estados.estado)
        })
      }else{
        this.form = new UntypedFormGroup({
           'id':new UntypedFormControl(0),
          'estado':new UntypedFormControl('')
        })
      }
    })
    this.httpClientService.mensaje.subscribe(data=>{
      this.snack.open(data,null,{duration:2000})
    })
  }
  validar(campo:string){
    return this.form.get(campo)?.invalid &&
    this.form.get(campo)?.touched
  }

  registrar(){
    this.estados.estado = this.form.value['estado'];

    this.httpClientService.addEstado(this.estados).subscribe(data=>{
      if(data===1){
        this.estadoAddedEvent.emit();
        this.router.navigate(['admin','estado']);
        this.httpClientService.mensaje.next('Se agrego Correctamente');
      }else{
        this.httpClientService.mensaje.next("No se agrego Correctamente");
      }
    })
  }
  modificar(){
    this.estados.estado = this.form.value['estado'];
    this.httpClientService.updateEstado(this.estados).subscribe(data=>{
      this.estadoAddedEvent.emit();
      this.router.navigate(['admin','estado']);
      this.httpClientService.mensaje.next("Se modifico Correctamente");
    })
  }

  saveEstado(){
    if(!this.form.valid){
      alert('Ingrese Valores');
      return;
    }

    if(this.estados.id==null){
      this.registrar()
    }else{
      this.modificar();
    }
  }
}
