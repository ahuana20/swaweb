import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { EstadoModel } from '../../../../_model/estado.model';
import { EstadoService } from '../../../../service/estado.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-viewestado',
  templateUrl: './viewestado.component.html',
  styleUrls: ['./viewestado.component.css']
})
export class ViewestadoComponent implements OnInit {
  @Input()
  estados:EstadoModel

  @Output()
  estadoDeletedEvent = new EventEmitter();
  constructor(private httpClientService:EstadoService,
              private router:Router,
              private activedRoute:ActivatedRoute,
              private snack:MatSnackBar) { }

  ngOnInit(): void {
    this.httpClientService.mensaje.subscribe(data=>{
      this.snack.open(data,null,{duration:2000})
    })
  }
  deleteEstado(){
    this.httpClientService.deleteEstado(this.estados.id).subscribe(book=>{
      this.estadoDeletedEvent.emit();
      this.router.navigate(['admin','estado']);
    })
  }
  editEstado(){
    this.router.navigate(['admin','estado'],{skipLocationChange:true,queryParams:{action:'edit',id:this.estados.id}})
  }
}
