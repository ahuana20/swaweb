import { ModalidadPago } from './../../../../_model/modalidadPago.model';
import { Component, Input, OnInit, EventEmitter, Output } from '@angular/core';
import { ProductoModel } from '../../../../_model/producto.model';
import { ProductoService } from '../../../../service/producto.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { UntypedFormControl, UntypedFormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef } from '@angular/material/dialog';
import { ModalidadService } from '../../../../service/modalidad.service';



@Component({
  selector: 'app-addmodalidad',
  templateUrl: './addmodalidad.component.html',
  styleUrls: ['./addmodalidad.component.css']
})
export class AddmodalidadComponent implements OnInit {
  @Input()
  productos: ModalidadPago;
  form:UntypedFormGroup;
  private selectedFile;
  imgURL: any;
  @Output()
  productoAddedEvent = new EventEmitter();
  validarnombres:string ="^[áéíóúÁÉÍÓÚaA-zZ\\s]*"
  constructor(private httpClientService: ModalidadService,
    private activedRoute: ActivatedRoute,
    private router: Router,
    private httpClient: HttpClient,
    private snack:MatSnackBar,
   ) {

      this.productos = new ProductoModel();
      this.form = new UntypedFormGroup({
        'id': new UntypedFormControl(0),
        'name': new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarnombres)]),
      
      })   ;


     }

  ngOnInit() {
  
      this.activedRoute.queryParams.subscribe(data=>{
        if(data['action']=='edit'){
          this.form = new UntypedFormGroup({
            'name': new UntypedFormControl(this.productos.name),
            'price': new UntypedFormControl(this.productos.name),
         
                       })  ; 
             }else{
            this.form = new UntypedFormGroup({
              'id': new UntypedFormControl(0),
              'name': new UntypedFormControl(''),
            
           
            })   ;
          }
      })
      this.httpClientService.mensaje.subscribe((data)=>{
        this.snack.open(data,null,{duration:2000});
      })
  
  }
  validar(campo:string){
    return this.form.get(campo)?.invalid &&
    this.form.get(campo)?.touched
  }

  public onFileChanged(event) {
    console.log(event);
    this.selectedFile = event.target.files[0];

    let reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (event2) => {
      this.imgURL = reader.result;
    };
  }

  registrar(){
   
      this.productos.name = this.form.value['name'];
 
      this.httpClientService.addPago(this.productos).subscribe(data=>{
          if(data===1){
            this.productoAddedEvent.emit();
            this.router.navigate(['admin', 'modalidad']);
            this.httpClientService.mensaje.next("Se Agrego Correctamente");
          
          }else{
            this.httpClientService.mensaje.next("No se Agrego Correctamente");
          }
    
  } );    
  


  }
  
  modificar(){
    this.productos.name = this.form.value['name'];

    this.httpClientService.updateModalidad(this.productos).subscribe(producto=>{
      this.productoAddedEvent.emit();
      this.router.navigate(['admin', 'modalidad']);
      this.httpClientService.mensaje.next("Se Modifico Correctamente");
    })
  }
  saveProducto(){
    if(!this.form.valid){
      alert('Ingrese Valores');
      return;
    }

    if (this.productos.id == null) {
    this.registrar()
    }else{
      this.modificar();
    }}
  


  }

/*  saveProducto() {
    this.productos.name = this.form.value['name'];
    this.productos.price = this.form.value['price'];
    this.productos.tipo = this.form.value['tipo'];
    this.productos.unidad = this.form.value['unidad'];
    if (this.productos.id == null) {
    const uploadData = new FormData();
    uploadData.append('imageFile', this.selectedFile, this.selectedFile.name);
    this.selectedFile.imageName = this.selectedFile.name;
 
    this.httpClient.post('http://localhost:8081/productos/upload', uploadData, { observe: 'response' })
      .subscribe((response) => {
        if (response.status === 200) {
          this.httpClientService.addProductos(this.productos).subscribe(
            (book) => {
              this.productoAddedEvent.emit();
              this.router.navigate(['admin', 'productos']);
              this.httpClientService.mensaje.next("Se Agrego Correctamente");
            }
          );
          console.log('Image uploaded successfully');
        } else {
          console.log('Image not uploaded successfully');
        }
      }
      );
  } else {
    this.httpClientService.updateProducto(this.productos).subscribe(
      (book) => {
        this.productoAddedEvent.emit();
        this.router.navigate(['admin', 'productos']);
        this.httpClientService.mensaje.next("Se Modifico Correctamente");
      }
    );
  }

}
*/




  



  
