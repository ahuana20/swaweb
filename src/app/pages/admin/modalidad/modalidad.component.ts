import { Component, OnInit } from '@angular/core';
import { ProductoModel } from '../../../_model/producto.model';
import { ProductoService } from '../../../service/producto.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';


import { ModalidadPago } from '../../../_model/modalidadPago.model';
import { ModalidadService } from '../../../service/modalidad.service';

@Component({
  selector: 'app-modalidad',
  templateUrl: './modalidad.component.html',
  styleUrls: ['./modalidad.component.css']
})
export class ModalidadComponent implements OnInit {
  productos: Array<ModalidadPago>;
  selectedProducto: ModalidadPago;
  productoRecieved: Array<ModalidadPago>;
  dataSource:MatTableDataSource<ModalidadPago>;
  displayedColumns= ["idProducto","nombre","Acciones"];
  action: string;
  constructor(private httpClientService: ModalidadService,
    private activedRoute: ActivatedRoute,
    private router: Router,
    private dialog:MatDialog) { }

  ngOnInit() {
    this.refreshData();

  }
  refreshData() {
    this.httpClientService.getModalidad().subscribe(
      response => 
     
      this.handleSuccessfulResponse(response)
         
    );

    this.activedRoute.queryParams.subscribe(
      (params) => {
        this.action = params['action'];
        console.log(this.action)
        const id = params['id'];
        if (id) {
          this.selectedProducto = this.productos.find(book => {
            return book.id === +id;
          });
      }
    }
    );
    }
  handleSuccessfulResponse(response) {
    this.productos = new Array<ModalidadPago>();
    this.productoRecieved = response;
    for (const produ of this.productoRecieved) {
      const productowithRetrievedImageField = new ModalidadPago();
      productowithRetrievedImageField.id = produ.id;
      productowithRetrievedImageField.name = produ.name;

      this.productos.push(productowithRetrievedImageField);
     
     
    }
    this.dataSource = new MatTableDataSource(this.productos);
  }
  addProducto() {
    this.selectedProducto = new ProductoModel();
    this.router.navigate(['admin', 'modalidad'], {skipLocationChange:true, queryParams: { action: 'add' } });
  }
  viewProducto(id: number) {
  
   this.router.navigate(['admin','modalidad'], {skipLocationChange:true, queryParams: { id, action: 'view' } });
 
  }
 

  

}
