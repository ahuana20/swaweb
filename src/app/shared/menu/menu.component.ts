import { Component, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { LoginService } from '../../service/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  @ViewChild(MatMenuTrigger) trigger: MatMenuTrigger;
  opened: boolean=false;
  events: string[] = [];
  shouldRun = [/(^|\.)plnkr\.co$/, /(^|\.)stackblitz\.io$/].some(h => h.test(window.location.host));
  constructor(public login:LoginService,
            public router:Router) {
      
   }

  ngOnInit(): void {
  }
  someMethod() {
    this.trigger.openMenu();
  }
  cerrarSesion(){
    sessionStorage.clear();
  }
  miperfil(){
    this.router.navigate(['/profile'])
  }
}
