import { Component, OnInit } from '@angular/core';
import { UserService } from '../../service/user.service';
import { ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PasswordValidation } from './token/match';
import { RecuperarPasswordService } from '../../service/recuperarPassword.service';

@Component({
  selector: 'app-recuperar',
  templateUrl: './recuperar.component.html',
  styleUrls: ['./recuperar.component.css']
})
export class RecuperarComponent implements OnInit {
 
  email:string
  mensaje:string
  error:string;
  porcentaje:number=0
  
  constructor(private login:RecuperarPasswordService,public route:ActivatedRoute) { 
 
  }

  ngOnInit(): void {
    
  }
  enviar(){
    this.porcentaje=99;
    this.login.enviarCorreo(this.email).subscribe(data=>{
      if(data===1){
        this.mensaje="Se enviaron las indicaciones al correo."
        this.error=null;
        this.porcentaje=100;

      }else{
        this.error="El usuario ingresado no existe";
        this.porcentaje=0;
      }
    },(error)=>{
      this.error="Error al Enviar";
      this.porcentaje=0;
    })
  }
}
