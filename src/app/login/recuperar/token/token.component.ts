import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup } from '@angular/forms';
import { PasswordValidation } from './match';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../../service/user.service';
import { RecuperarPasswordService } from '../../../service/recuperarPassword.service';

@Component({
  selector: 'app-token',
  templateUrl: './token.component.html',
  styleUrls: ['./token.component.css']
})
export class TokenComponent implements OnInit {
  form:UntypedFormGroup;
  token:string;
  mensaje:string;
  error:string;
  rpta:number;
  tokenValido:boolean;
  constructor(private fb:UntypedFormBuilder,private router:Router,private route:ActivatedRoute,private userService:RecuperarPasswordService) { 
    this.form = fb.group({
      password:[''],
      confirmPassword:['']
    },{
      validator:PasswordValidation.MatchPassword
    })
  }

  ngOnInit(): void {
  this.route.params.subscribe((params:Params)=>{
    this.token = params['token'];
    this.userService.verificartokenReset(this.token).subscribe(data=>{
      if(data===1){
        this.tokenValido=true;
      }
      else{
        this.tokenValido=false; 
        setTimeout(()=>{
          this.router.navigate(['login']);
        },2000)
      }
    })
  })
  }
  onSubmit(){
    let clave :string=this.form.value.confirmPassword;
    this.userService.restablecer(this.token,clave).subscribe(data=>{
      if(data===1){
        this.rpta=1;
      }
    },(err=>{
      this.rpta=0;
    }))
  }
}
