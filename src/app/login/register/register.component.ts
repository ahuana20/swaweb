import { Component, OnInit } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { UserModel } from '../../_model/user.model';
import { UserService } from '../../service/user.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { LoginService } from '../../service/login.service';
import { TOKEN_NAME } from '../../local/var_constant';
import {default as decode} from 'jwt-decode';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user:UserModel;
  form:UntypedFormGroup;
  error:string="";
  validarnombres:string ="^[áéíóúÁÉÍÓÚaA-zZ\\s]*"
  validarcelular:string="^[9]{1}\\d{8}$"
  constructor(private service:UserService,private router:Router,private login:LoginService) { 
    this.user = new UserModel();
    this.form = new UntypedFormGroup({
      'id': new UntypedFormControl(0),
      'apellidos':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarnombres)]),
      'celular':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarcelular)]),
      'email':new UntypedFormControl('',[Validators.required,Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]),
      'password':new UntypedFormControl('',Validators.required),
      'username':new UntypedFormControl('', [Validators.required, Validators.pattern("^[A-Za-z0-9]*")]),
      'nombres':new UntypedFormControl('',[Validators.required,Validators.pattern(this.validarnombres)])
    })
  }

  ngOnInit(): void {
  }

  register(){
      if(this.form.valid){
        this.operar();
      }
      else{
        alert('Ingrese Valores')
        
        return;
      }
  }

  operar(){
    this.user.apellidos = this.form.value['apellidos'];
    this.user.celular = this.form.value['celular'];
    this.user.correo = this.form.value['email'];
    this.user.password = this.form.value['password'];
    this.user.username = this.form.value['username'];
    this.user.nombres = this.form.value['nombres'];
    this.service.register(this.user).subscribe(data=>{
        { 
          if(data){
           
            this.login.login(this.user.username,this.user.password).subscribe(data=>{
              if(data){
             let token =JSON.stringify(data);
             sessionStorage.setItem(TOKEN_NAME,token);
             let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
             const decodeToken = decode(tk.access_token);
             let rol = decodeToken['authorities'][0];
              if(rol==="ROLE_ADMIN"){
               this.router.navigate(['admin','productos']);
               Swal.fire(
                 `Bienvenido  ${this.user.username}!`,
                 `Eres un : ${rol}`,
                 'success'
               )
              }else{
                this.router.navigate(['shop'])
                Swal.fire(
                 `Bienvenido  ${this.user.username}!`,
                 `Gracias Por Registrarse`,
                 'success'
               )
              }
              }
           },(error)=>{
             if(error.status !==401){
               this.error="Error de Conexion";
               Swal.fire({
                 icon: 'error',
                 title: 'Oops...',
                 text: `${this.error}`,
                
               })
             }
             if(error.status===401|| error.status===400){
               this.error ="Credenciales Incorrectas";
                 Swal.fire({
                 icon: 'error',
                 title: 'Oops...',
                 text: `${this.error}`,
                
               })
             }
           }
           )
          
         
          }else{
            alert('el usuario ya esta registrado')
            return;
          }
        }
        },(error)=>{
          if(error.status===500){
               alert('El correo ya fue Registrado');
               return;
          }
        }
        )
  }
  validar(campo:string){
    return this.form.get(campo)?.invalid &&
    this.form.get(campo)?.touched
  }
  validate(control:UntypedFormControl){

  }
}
