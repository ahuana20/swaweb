import { Component, OnInit, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';
import { TOKEN_NAME } from '../local/var_constant';

import {default as decode} from 'jwt-decode';
import { UserService } from '../service/user.service';
import Swal from 'sweetalert2';
//import { SocialAuthService, SocialUser } from "angularx-social-login";
//import {  GoogleLoginProvider } from "angularx-social-login";
import { GmailService } from '../service/gmail.service';
import { TokenService } from '../service/token.service';
import { TokenDTO } from '../_model/TokenDTO.model';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 //SocialUser:SocialUser;
//userLogged:SocialUser;
isLogged:boolean

usuario:string;
password:string;
error:string="";
  constructor(private service:LoginService,private router:Router,
    public login:LoginService,
      private user:UserService,
      //private authService: SocialAuthService,
      private gmal:GmailService,
      private token:TokenService) { }

  ngOnInit(): void {
    if(this.login.estaLogeado()){
      this.router.navigate(['shop'])
    }
   /* this.authService.authState.subscribe(data=>{
      this.userLogged=data;
      this.isLogged =(this.userLogged !=null && this.token.getToken()!=null)
    })
*/
    console.log(JSON.parse(sessionStorage.getItem(TOKEN_NAME)));
  }


  iniarSesion(){
    this.service.login(this.usuario,this.password).subscribe(data=>{
       if(data){
      let token =JSON.stringify(data);
      sessionStorage.setItem(TOKEN_NAME,token);
      let tk = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
      const decodeToken = decode(tk.access_token);

      let rol = decodeToken['authorities'][0];
      console.log(rol);
       if(rol==="ROLE_ADMIN"){
        this.router.navigate(['admin','productos']);
        Swal.fire(
          `Bienvenido  ${this.usuario}!`,
          `Eres un : ${rol}`,
          'success'
        )
       }else{
         this.router.navigate(['shop'])
         Swal.fire(
          `Bienvenido  ${this.usuario}!`,
          `Eres un : ${rol}`,
          'success'
        )
       }
       }
    },(error)=>{
      if(error.status !==401){
        this.error="Error de Conexion";
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: `${this.error}`,

        })
      }
      if(error.status===401|| error.status===400){
        this.error ="Credenciales Incorrectas";
          Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: `${this.error}`,

        })
      }
    }
    )

  }
  saberQuienEs(){
   this.user.getId().subscribe(data=>{
    console.log(data);
   })

  }



  cerrarSesion(){
    sessionStorage.clear();
    this.router.navigate(['login']);

   /* if(this.isLogged){
     this.authService.signOut().then(data=>{
       this.token.logOut();
       this.isLogged=false;
     })
    }*/

  }
  iniarSesionGMAIL(){
  /*  this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(
      data => {
        this.SocialUser = data;
        const tokenGoogle = new TokenDTO(this.SocialUser.idToken);
        this.gmal.google(tokenGoogle).subscribe(
          res => {
            this.token.setToken(res.value);
            this.isLogged = true;
            this.router.navigate(['shop']);
          },
          err => {
            console.log(err);
            this.cerrarSesion();
          }
        );
      }
    ).catch(
      err => {
        console.log(err);
      }
    );*/
  }
}
