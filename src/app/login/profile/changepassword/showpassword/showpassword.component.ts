import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { UserModel } from '../../../../_model/user.model';
import {default as decode} from 'jwt-decode';
import { TOKEN_NAME } from '../../../../local/var_constant';
import { UserService } from '../../../../service/user.service';
@Component({
  selector: 'app-showpassword',
  templateUrl: './showpassword.component.html',
  styleUrls: ['./showpassword.component.css']
})
export class ShowpasswordComponent implements OnInit {
  @Input()
  user:UserModel



  @Output() change = new EventEmitter();
  constructor(private login:UserService) { }

  ngOnInit(): void {
  }  
  changePassword(){
      this.change.emit();
     
  }

}
