import { TOKEN_NAME } from './../../../local/var_constant';
import { Component, OnInit } from '@angular/core';
import { UserModel } from '../../../_model/user.model';
import {default as decode} from 'jwt-decode';
import { UserService } from '../../../service/user.service';
import { UntypedFormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { UpdatePassword } from '../../../_model/UpdatePassword';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-changepassword',
  templateUrl: './changepassword.component.html',
  styleUrls: ['./changepassword.component.css']
})
export class ChangepasswordComponent implements OnInit {
  user:UserModel;
  mostrar:boolean=false;
  form:UntypedFormGroup;
  constructor(private login:UserService,
              ) { }

  ngOnInit(): void {
    this.verificarUsuario();
    this.form = new UntypedFormGroup({
      'password':new UntypedFormControl('',[Validators.required]),
      'conpassword':new UntypedFormControl('',[Validators.required])

    })
  }
  verificarUsuario(){
    let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodeToken = decode(token.access_token);
      let rol = decodeToken['user_name'];
    
        let c:number;
        this.login.obtenerUsuario(rol).subscribe(data=>{
       this.user=data;
       
     
      })
  }
  cambiar(){
    this.mostrar=true;
    console.log(this.mostrar)
  }

  cambiarPassword(){
  this.insertarNuevaPassowrd()
 /*     Swal.fire({
        title: '¿Quieres Realizar Los Cambios?',
        confirmButtonText: `Save`,
        showCancelButton: true,
        
      }).then(function (result) {
        
        if (result.value) {
          Swal.fire('Cambios Correctos!', '', 'success')
       
        } else {
          Swal.fire('Los cambios no se realizaron', '', 'info')
        }
      })
    */
  
}
  insertarNuevaPassowrd(){
    let u = new UpdatePassword();
    console.log('cambiado');
    let pas =this.form.value['password'];
    let conp=this.form.value['conpassword'];
    u.id= this.user.idUsuarios
    u.password=pas;
    console.log(u);
    if(conp==null || conp==undefined|| !conp && pas==null ||pas==undefined || !pas){
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Por favor Llene los campos Solicitados',
       
      })
      return;
    }
    if(conp==pas){
      console.log(`${pas} , ${conp}`)
      this.login.updatePassword(u).subscribe(data=>{
        Swal.fire('Cambios Correctos!', '', 'success')
        this.mostrar=false;
  })
    }
    else{
      Swal.fire('Por favor Llene los campos Solicitados')
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Por favor Llene los campos Solicitados',
       
      })
      this.mostrar=false;
    }
  }
}
