import { TOKEN_NAME } from './../../../local/var_constant';
import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { carList } from '../../../_model/carListInterface';
import { UntypedFormGroup } from '@angular/forms';
import {default as decode} from 'jwt-decode';
import { UserService } from '../../../service/user.service';
import { UserModel } from '../../../_model/user.model';
import { CarService } from '../../../service/car.service';
import { buscarID } from '../../../_model/buscarId';
@Component({
  selector: 'app-mybuy',
  templateUrl: './mybuy.component.html',
  styleUrls: ['./mybuy.component.css']
})
export class MybuyComponent implements OnInit {
  form:UntypedFormGroup;
  user:UserModel;
  car:carList[]
  displayedColumns: string[] = ['position', 'name', 'weight', 'symbol','nombrescompletos','modalidad','productos','estado'];
  dataSource:MatTableDataSource<carList>;
  constructor(private login:UserService,private carService:CarService) { }

  ngOnInit(): void {
    this.verificarUsuario();
    //this.llenarTable();
  }
  verificarUsuario(){
    let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodeToken = decode(token.access_token);
      let rol = decodeToken['user_name'];
        let c:number;
       this.login.obtenerUsuario(rol).subscribe(data=>{
       this.user=data;
         console.log(this.user)
         this.llenarTable()
      })
  }

  llenarTable(){
    let u = new buscarID();
    u.id = this.user.idUsuarios
    console.log(this.user)
    if(u.id){
      this.carService.listarPorid(u).subscribe(data=>{
        
        this.forEach(data);
        this.dataSource= new MatTableDataSource(this.car)
        console.log(this.car);
      })
    }
  }


  forEach(data){
    this.car=[]
    for (const produ of data) {
      const d = new carList();
      d.id_car = produ[0]
      d.cantidadTotal=produ[1]
      d.fecha=produ[2]
      d.montototal=produ[3]
      d.username=produ[4]
      d.nombres=produ[5]
      d.modalidad=produ[6]
      d.cantidadproductos=produ[7]
      d.estado=produ[8]
      this.car.push(d)
     
  }
}
}
