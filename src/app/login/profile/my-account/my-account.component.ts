import { Component, Input, OnInit } from '@angular/core';
import { UserModel } from '../../../_model/user.model';
import { UserService } from '../../../service/user.service';
import { TOKEN_NAME } from '../../../local/var_constant';
import {default as decode} from 'jwt-decode';
@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {
  constructor(private login:UserService) { }

  user:UserModel;

  
  displayedColumns= ["nombres","correo","celular"];

  ngOnInit(): void {
    this.verificarUsuario();
  }

  verificarUsuario(){
    let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
    const decodeToken = decode(token.access_token);
      let rol = decodeToken['user_name'];
    
        let c:number;
        this.login.obtenerUsuario(rol).subscribe(data=>{
       this.user=data;
       console.log(this.user);
     
      })
  }
}
