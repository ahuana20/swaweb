

import { Injectable } from '@angular/core';
import { ActivatedRoute, RouterStateSnapshot, Router, CanActivate, UrlTree, ActivatedRouteSnapshot } from '@angular/router';


import { tokenNotExpired } from 'angular2-jwt';


import { LoginService } from './login.service';
import { TOKEN_NAME } from '../local/var_constant';
import {default as decode} from 'jwt-decode';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class GuardService  {

  constructor( private router:Router,private service:LoginService) {
   
   }
 
 
  canActivate(route: ActivatedRouteSnapshot,state: RouterStateSnapshot) {
  
    let rpta = this.service.estaLogeado();
    
      if(!rpta){
        sessionStorage.clear();
        this.router.navigate(['login']);
        return false;
      }else{
        let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME));
        if(tokenNotExpired(TOKEN_NAME,token.access_token)){
          const decodeToken = decode(token.access_token);
           
          let rol = decodeToken['authorities'][0];
          let url = state.url;
     
               switch(rol){
              case 'ROLE_ADMIN':{
               if(url==="/admin/productos" || url==='/admin/modalidad' || url ==='/admin/users'|| url==="/admin/estado" || url==='/admin/listadoProducto'|| url==="/profile"||url==="/profile/changepassword" || url==="/profile/my-account"||url==="/profile/my-buy" ){
                  return true;
               }else{
                  this.router.navigate(['NOT-403']);
                  return false;
               }
              }
              case 'ROLE_USER':{
                if(url==="/profile" || url==="/profile/changepassword" || url==="/profile/my-account" || url==="/profile/my-buy"){
                    return true;
                }else{
                  this.router.navigate(['NOT-403']);
                  return false;
                }
              }
              default:{
                this.router.navigate(['NOT-403']);
                break;
              }
            }
        }else{
          Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Su Sesion Expiro,Por favor Ingrese Nuevamente!',
           
          })
         
          sessionStorage.clear();
          this.router.navigate(['login']);
          return false;
        }
      }
}

}
