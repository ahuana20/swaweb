import { HOST } from './../local/var_constant';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserModel } from '../_model/user.model';
import { TOKEN_NAME } from '../local/var_constant';

import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UpdatePassword } from '../_model/UpdatePassword';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  mensaje = new Subject<string>();
  URL:string=`${HOST}/users`;
  constructor(private httpClient:HttpClient,
    private snack:MatSnackBar) { 

  }

  getUsers()
  {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.get<UserModel[]>(`${this.URL}/get`,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  addUser(newUser: UserModel) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.post<UserModel>(`${this.URL}/add`, newUser,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  deleteUser(id) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.delete<UserModel>(`${this.URL}/` + id,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  updateProducto(updatedBook :UserModel) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.put<UserModel>(`${this.URL}/update`, updatedBook,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  updatePassword(updatePassword:UpdatePassword){
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.put<UserModel>(`${this.URL}/updatePassword`, updatePassword,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
getId() {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.get(`${this.URL}/getId`,{
      responseType: "text" as "json", headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
     }).pipe(map(res=>{
  
        return res;

    }))
  }
    
 /* getIDBASE(id){
    return this.httpClient.get(`http://localhost:8081/listar/`+id)
  }*/
  register(newUser: UserModel){
    return this.httpClient.post<UserModel>(`${HOST}/listar/register`, newUser);
  
  }
  obtenerUsuario(username){
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.get<UserModel>(`${this.URL}/buscar/`+username,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }

}