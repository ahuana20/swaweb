import { HOST } from './../local/var_constant';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs';
import { TOKEN_NAME } from '../local/var_constant';
import { EstadoModel } from '../_model/estado.model';

@Injectable({
  providedIn: 'root'
})
export class EstadoService {
  mensaje = new Subject<string>();
  URL:string=`${HOST}/estado`
  constructor(private httpClient:HttpClient) { 

  }
  getEstado(){
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
   return this.httpClient.get<EstadoModel[]>(`${this.URL}/listar`,{
     headers:new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   })
  }

  addEstado(newEstado:EstadoModel){
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.post(`${this.URL}/add`, newEstado,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  deleteEstado(id) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.delete(`${this.URL}/` + id,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  updateEstado(update :EstadoModel) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.put<EstadoModel>(`${this.URL}/update`, update,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
}
