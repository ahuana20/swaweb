import { HOST } from './../local/var_constant';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserModel } from '../_model/user.model';
import { TOKEN_NAME } from '../local/var_constant';

import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { UpdatePassword } from '../_model/UpdatePassword';

@Injectable({
  providedIn: 'root'
})

export class RecuperarPasswordService {

    URL:string=`${HOST}/recuperar`;
    constructor(private httpClient:HttpClient,
      ) { 
  
    }


  enviarCorreo(correo:string){
    console.log(`${this.URL}/enviarCorreo`,correo);
    return this.httpClient.post<number>(`${this.URL}/enviarCorreo`,correo,{
      headers:new HttpHeaders().set('Content-Type','text/plain')});
      
}
verificartokenReset(token:string){
  return this.httpClient.get<number>(`${this.URL}/restablecer/verificar/${token}`);
}
restablecer(token:string,clave:string){
  return this.httpClient.post<number>(`${this.URL}/restablecer/${token}`,clave,{
   headers:new HttpHeaders().set('Content-Type','application/json') 
  })
}
}