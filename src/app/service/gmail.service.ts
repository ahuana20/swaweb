import { HOST } from './../local/var_constant';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenDTO } from '../_model/TokenDTO.model';
import { Observable } from 'rxjs';

const headers = {headers: new HttpHeaders({'Content-Type':'application/json'})}

@Injectable({
  providedIn: 'root'
})
export class GmailService {
  gmailUrl = `${HOST}/gmail`
  constructor(private http:HttpClient) {

   }
   google(token:TokenDTO):Observable<TokenDTO>{
     
     return this.http.post<TokenDTO>(`${this.gmailUrl}/google`,token,headers);
   }
}
