import { HOST } from './../local/var_constant';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ProductoModel } from '../_model/producto.model';
import { Subject } from 'rxjs';
import { TOKEN_NAME } from '../local/var_constant';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  mensaje = new Subject<string>();
  URL:string=`${HOST}/productos`;
  constructor(private httpClient:HttpClient) { }

  getProductos() {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.get<ProductoModel[]>(`${this.URL}/get`,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  getProductFree() {
    
    return this.httpClient.get<ProductoModel[]>(`http://localhost:8081/listar/get`);
  }
  getProductFreeId(id:number) {
    
    return this.httpClient.get<ProductoModel>(`http://localhost:8081/listar/listarId/${id}`);
  }



  addProductos(newProduct: ProductoModel) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.post(`${this.URL}/add`, newProduct,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  deleteProductos(id) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.delete(`${this.URL}/` + id,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  updateProducto(updatedBook :ProductoModel) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.put<ProductoModel>(`${this.URL}/update`, updatedBook,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  subirImagen(uploadData:FormData){
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
  return this.httpClient.post(`${this.URL}/upload`,uploadData,{
    observe: 'response',headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`)
   });
  }
  
  }
  