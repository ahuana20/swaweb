import { Injectable } from '@angular/core';
import { HOST, TOKEN_NAME } from '../local/var_constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ShopCarModel } from '../_model/car.mode';
import { ConsultaListaProducto } from '../_model/consultaListaProducto.model';
import { FiltroCompra } from '../_model/filtroConsulta.model';
import { carList } from '../_model/carListInterface';
import { updateCar } from '../_model/UpdateCar';
import { Subject } from 'rxjs';
import { buscarID } from '../_model/buscarId';

@Injectable({
  providedIn: 'root'
})
export class CarService {
  carCambio = new Subject<carList[]>();
  private URL:string = `${HOST}/carProducto`;
  constructor(private httpClient:HttpClient) {


   }

  buscar(FiltroCompra:FiltroCompra){
    console.log(FiltroCompra);
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.post<[]>(`${this.URL}/buscar`,FiltroCompra,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
    })
  }

  /* getCar(){
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.get<ShopCarModel[]>(`${this.URL}/listar`,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }*/
   addCar(nuevoPago: ConsultaListaProducto){
     console.log(nuevoPago);
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
   return this.httpClient.post<number>(`${this.URL}/add`, nuevoPago,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });


   }
 

   getListarProducto(){
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.get<carList[]>(`${this.URL}/listar`,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
   }
  updateCar(updateCar:updateCar){
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.put<[]>(`${this.URL}/updateEstado`,updateCar,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
    })
  }
  listarPorid(buscarId:buscarID){
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.post<[]>(`${this.URL}/buscarporID`,buscarId,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
    })
  }

}
