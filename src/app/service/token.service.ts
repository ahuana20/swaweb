import { TOKEN_NAME } from './../local/var_constant';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor() {

   }
   public getToken(): string {
    return sessionStorage.getItem(TOKEN_NAME);
  }

  public setToken(token: string): void {
    sessionStorage.removeItem(TOKEN_NAME);
    sessionStorage.setItem(TOKEN_NAME, token);
  }

  logOut(): void {
    sessionStorage.clear();
  }
}
