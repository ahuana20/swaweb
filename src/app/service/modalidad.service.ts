import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { HOST, TOKEN_NAME } from '../local/var_constant';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ModalidadPago } from '../_model/modalidadPago.model';

@Injectable({
  providedIn: 'root'
})
export class ModalidadService {
  mensaje = new Subject<string>();
  URL:string=`${HOST}/modalidadPago`;
  constructor(private httpClient:HttpClient) { 

  }

  getModalidad(){
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.get<ModalidadPago[]>(`${this.URL}/get`,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  addPago(nuevoPago: ModalidadPago) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.post(`${this.URL}/add`, nuevoPago,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  deletePago(id) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.delete(`${this.URL}/` + id,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }

  updateModalidad(updatedBook :ModalidadPago) {
    let acces_token = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;
    return this.httpClient.put<ModalidadPago>(`${this.URL}/update`, updatedBook,{
      headers: new HttpHeaders().set('Authorization',`bearer ${acces_token}`).set('Content-Type','application/json')
   });
  }
  getModalidadFree(){
      return this.httpClient.get<ModalidadPago[]>(`http://localhost:8081/listar/getModalidad`)
  }

}