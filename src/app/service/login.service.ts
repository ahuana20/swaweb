import { HOST } from './../local/var_constant';

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TOKEN_AUTH_USERNAME, TOKEN_AUTH_PASSWORD, TOKEN_NAME } from '../local/var_constant';

import { Router } from '@angular/router';
import {default as decode} from 'jwt-decode';



@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private url:string = `${HOST}/oauth/token`;
  constructor(private http:HttpClient,private router:Router) { 

  }
  login(user:string,password:string){
    const body = `grant_type=password&username=${encodeURIComponent(user)}&password=${encodeURIComponent(password)}`;
   return this.http.post(this.url,body,{ 
    headers:new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded; charset=UTF-8').set
    ('Authorization','Basic '+ btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD))
  
  })
    
  /*  return new Promise((res,rej)=>{
      this.http.post(this.url,body,{ 
        headers:new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded; charset=UTF-8').set
        ('Authorization','Basic '+ btoa(TOKEN_AUTH_USERNAME + ':' + TOKEN_AUTH_PASSWORD))
      }).subscribe(response=>res(response),
        error=>rej(error)
      )
    })
    */
  }

    estaLogeado(){
      let token = sessionStorage.getItem(TOKEN_NAME);
       return token!=null;
    }
    cerrarSesion(){
      sessionStorage.clear();
    }
    
    isAdmin(){
      let tok = sessionStorage.getItem(TOKEN_NAME);
      if(tok!=null){
        const decodeToken = decode(tok);
     
        let isAdmin =  decodeToken['authorities'].some(el=>el==='ROLE_ADMIN');
  
        return isAdmin;
      
      }
    }
  
}

