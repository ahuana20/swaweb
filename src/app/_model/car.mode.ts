import { UserModel } from './user.model';
import { ModalidadPago } from './modalidadPago.model';
import { DetalleCar } from './detalle.model';
import { EstadoModel } from './estado.model';
export class ShopCarModel{
    public idCar:number;
    public cantidad:number;
    public montotal:number;
    public fecha:Date;
    public usuario:UserModel;
    public modalidadPago:ModalidadPago;
    public estado:EstadoModel
    public detalleCar:DetalleCar[];
    
 
}