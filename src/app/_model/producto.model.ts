export class ProductoModel {
    id: number;
    name: string;
    tipo: string;
    price: number;
    unidad:number;
    picByte: string;   
    retrievedImage: string; 
    isAdded: boolean;
    descripcion:string;
    cantidad:number;
    }